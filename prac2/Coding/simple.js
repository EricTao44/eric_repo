//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
//question4();
//question5();

function question1(){
    let output = "NegativeEven: "
    
    //empty output, fill this so that it can print onto the page.
    
    let myArray = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25]

    let negativeEvenArray = [];
    let positiveOddArray = [];
    
   
    
    for (let i = 0; i <= myArray.length; i++){
        if (myArray[i] < 0 && myArray[i] % 2 == 0){ 
            negativeEvenArray.push(myArray[i])
        }
        else if (myArray[i] > 0 && myArray[i] % 2 == 1){ 
            positiveOddArray.push(myArray[i])
        }
    }
    
  
    output += negativeEvenArray
    
    
    output += "\n Positive Odd: "
   
    
    output += positiveOddArray
    
    
   /* output += "Positive Odd: "  
        
    for(let i = 0; i <= negativeEvenArray.length; i++ ){
        output += negativeEvenArray[i] + " "
    }
    
    output += "Negative Even: "
        
    for(let i = 0; i <= positiveOddArray.length; i++ ){
        output += positiveOddArray[i] + " "
    }   */

    //Question 1 here 
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    
     //Question 2 here
    let output = "Frecuency of dice roll: "
    let randNum = 0;
    let oneNum = 0;
    let twoNum = 0;
    let threeNum = 0;
    let fourNum = 0;
    let fiveNum = 0;
    let sixNum = 0;
    
    
    for(i = 0; i <= 60000; i++){
        randNum = Math.floor((Math.random() * 6) + 1)
        if(randNum == 1){
            oneNum ++
        }
        else if (randNum == 2){
            twoNum ++
        }
        else if (randNum == 3){
            threeNum ++
        }
        else if (randNum == 4){
            fourNum ++
        }
        else if (randNum == 5){
            fiveNum ++
        }
        else{
            sixNum ++
        }
        
    }
   
   output += "\n1: " + oneNum + "\n2: " + twoNum + "\n3: " + threeNum + "\n4: " + fourNum + "\n5: " + fiveNum + "\n6: " + sixNum
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    
    //Question 3 here 
    let output = " "
    let myArray = [0,0,0,0,0,0,0]
    
    for(i = 0; i <= 60000; i++){
        randNum = Math.floor((Math.random() * 6) + 1)
        myArray[randNum] ++
        
     }
     
    output += myArray
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}